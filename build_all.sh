#!/bin/bash

declare -A desktops

distro=$(grep distro config/settings.cfg | sed s/distro=//)
release=$(grep release config/settings.cfg | sed s/release=//)
workdir=$(grep workdir config/settings.cfg | sed s/workdir=//)
destdir=/home/bcd/public_html/isos/trial-builds

archs="i586 x86_64"

desktops[i586]="Xfce"

desktops[x86_64]="GNOME Plasma Xfce"

case $(hostname) in
  *.mageia.org)  sudo_root="sudo -u draklive sudo"; sudo_bcd="sudo -u bcd";;
esac

./update_bootloader_files.sh
for arch in $archs; do
  for desktop in ${desktops[$arch]}; do
    isoname=$distro-$release-Live-$desktop-$arch
    $sudo_root draklive2 --clean --all --define arch=$arch --define desktop=$desktop
    $sudo_bcd  mkdir -p $destdir/$isoname
    $sudo_bcd  cp $workdir/$isoname/dist/* $destdir/$isoname/
    # not enough space on rabbit's RAM disk for all four variants, so delete as we go
    $sudo_root draklive2 --clean --define arch=$arch --define desktop=$desktop
  done
done
