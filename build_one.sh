#!/bin/sh

case $(hostname) in
  *.mageia.org)  sudo="sudo -u draklive sudo";;
esac

./update_bootloader_files.sh
if [ $# -gt 0 ] ; then
  $sudo draklive2 $*
else
  $sudo draklive2 --clean --all
fi
