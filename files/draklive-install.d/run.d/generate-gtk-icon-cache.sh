#!/bin/sh

for icondir in /usr/share/icons/*; do 
	if [ -d "$icondir" -a -f "$icondir/index.theme" ]; then 
		/usr/bin/gtk-update-icon-cache --force --quiet "$icondir";
	fi
done
