#!/bin/sh

chroot=$1
name=$2

if [ -z "$chroot" ] || [ -z "$name" ]; then
    echo "usage: $0 <chroot> <name>"
fi

full=$name-full.lst
leaves=$name-leaves.lst
chroot $chroot sh -c "rpm -qa --qf '%{size}      \t%{name}\n' | sort -n" > $full
chroot $chroot sh -c "urpmi_rpm-find-leaves | xargs rpm -q --qf '%{size}       \t%{name}\n' | sort -n" > $leaves

echo $full $leaves
