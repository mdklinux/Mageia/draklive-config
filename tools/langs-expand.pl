#!/usr/bin/perl -l
use lib qw(/usr/lib/libDrakX);
use lang;
use MDK::Common;
my @langs = grep { member(lang::locale_to_main_locale($_), @ARGV) } lang::list_langs();
print join("\n", map { lang::l2name($_) . " (" . $_ . ")" } sort(@langs));
