#!/bin/sh

DEVICE=$1
LABEL=Share
if [ -z "$DEVICE" ]; then
    echo "No device"
    exit 1
fi

# FT
#END1=780
#END2=1950

#END1=380
#END2=1022

END1=25000
END2=60869

fdisk $DEVICE <<EOF
o
n
p
1

$END1
n
p
2

$END2
t
1
b
t
2
b
w
EOF

mkdosfs -n $LABEL ${DEVICE}1
