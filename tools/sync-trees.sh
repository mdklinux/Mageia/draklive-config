#!/bin/sh

#MEDIA=/media/non-free/release
[ -n "$1" ] && DRY_RUN=-n
DIST=/cooker/i586
ROOT=$DIST$MEDIA
SOURCE=/live/mnt/BIG/dis/devel$ROOT
DEST=/mnt/field/dis/local$ROOT
TEST=media/main/release/media_info/UPDATING

rsync $DRY_RUN -avP -lHz -e 'ssh -xc arcfour' --delete --delete-excluded --exclude 'debug_*/*/*' --exclude SRPMS $SOURCE/ $DEST

if [ -e "$DEST/$TEST" ]; then
    echo
    echo "Warning: package upload in progress"
    echo "Resync tree!"
    exit 1
fi

exit 0
