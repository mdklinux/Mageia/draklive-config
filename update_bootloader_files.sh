#!/bin/sh
repository=$(grep repository config/settings.cfg | sed s/repository=//)
echo "Fetching bootloader files from $repository"

# Remove old files
rm -rf bootloader
mkdir bootloader

archs="i586 x86_64"
for arch in $archs; do
  path=`ls -1v $repository/$arch/media/core/{release,updates}/drakiso-bootloader-files*.rpm 2> /dev/null | tail -1`
  if [ -z $path ] ; then
    echo "ERROR: couldn't find $arch drakiso-bootloader-files RPM in $repository."
    exit 1
  else
    echo "Extracting $arch bootloader files from repository."
    rpm2cpio $path | cpio -idm --quiet
    if [ $? -ne 0 ] ; then
      echo "ERROR: failed to extract files from archive."
      exit 1
    fi
  fi
  cp -ru usr/share/drakiso/bootloader/* bootloader
  rm -r usr
done
